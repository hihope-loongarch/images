/*
 * Copyright (c) 2020 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

export interface SocketInfo {
    /**
     * Bound IP address.
     */
    address: string;
    /**
     * Network protocol type. The options are as follows: IPv4, IPv6.
     */
    family: 'IPv4' | 'IPv6';
    /**
     * Port number. The value ranges from 0 to 65535.
     */
    port: number;
    /**
     * Length of the server response message, in bytes.
     */
    size: number;
}

export interface SocketRecvInfo {
    /**
     * UDP Client receive Msg from UDP Server.
     */
    recvMsg: string;
    /**
     * length of recvMsg.
     */
    recvMsgLen: number;
    /**
     * UDP Server addr.
     */
    fromAddr: string;
    /**
     * UDP Server addr.
     */
    fromPort: number;
}

/**
 * @Syscap SysCap.ACE.UIEngine
 */
export default class Socket {
  //int socket(int domain, int type, int protocol);
  //string domain [ipv4][ipv6]
  //string type   [TCP][UDP]
  //return sock_fd
  static socketOpen(options: {
    domain: string;
    type: string;
  }): number;

  //int close(int sock_fd);
  //return closing sock_fd OK[0] or NG[-1]
  static socketClose(options: {
    sock_fd: number;
  }): number;

  //int connect(int sock_fd, const socket* addr, socklen_t addrlen);
  //TCP client connect to server[dest_addr + dest_port]
  static socketBind(options: {
    sock_fd: number;
    dest_addr: string;
    dest_port: number;
  }): number;

  //ssize_t send(int sock_fd, const void* buf, size_t len, int flags);
  //TCP client send msg{buf[len]} to server connected by [sock_fd + dest_addr + dest_port]
  static socketSend(options: {
    sock_fd: number;
    buf: number;
    len: number;
    flags: number;
  }): number;

  //ssize_t sendto(int sock_fd, const void* buf, size_t len, int flags, const struct sockaddr* dest_addr, socklen_t addrlen);
  //UDP client send msg{buf[len]} to server connected by [sock_fd + dest_addr + dest_port]
  static socketSendto(options: {
    sock_fd: number;
    buf: string;
    len: number;
    flags: number;
    dest_addr: string;
    dest_port: number;
  }): number;

  //ssize_t recv(int sock_fd, void* buf, size_t max_len, int flags);
  //TCP client receive msg{SocketRecvInfo} from server connected by [sock_fd + dest_addr + dest_port],
  //and the msg.xxx info is in SocketRecvInfo
  static socketRecv(options: {
    sock_fd: number;
    max_len: number;
  }): SocketRecvInfo;
  
  //ssize_t recvfrom(int sock_fd, void* buf, size_t max_len, int flags, struct sockarrd* src_addr, socklen_t* addrlen);
  //UDP client receive msg{SocketRecvInfo} from server connected by [sock_fd],
  //and the msg.xxx info is in SocketRecvInfo
  static socketRecvfrom(options: {
    sock_fd: number;
    max_len: number;
  }): SocketRecvInfo;
}
