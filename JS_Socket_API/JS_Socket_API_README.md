# 龙芯适配OH小型系统JS应用Socket接口说明

liangkz 2023.05.16



## JS应用如何使用Socket接口

DevEco Studio升级OpenHarmony SDK到API8或API9之后，主推以**ARKTS语言**为主的**标准系统应用**开发能力，看上去不再推**JS语言**的应用开发能力，为此，OH小型系统要用JS语言开发应用程序，需要回退到API7【或者也有可以用API8或API9开发可以在OH小型系统上运行的JS应用程序，请自行确认和使用即可】。

<img src="figures/1-OhosSdkAPI7.png" alt="1-OhosSdkAPI7" style="zoom:50%;" />

如上图所示，在IDE主页的Configure选项中打开Settings选项，在SDK项目中选择OpenHarmony，在Location选项中选择SDK的保存路径，在API版本列表中选择API7的JS接口并下载安装。

<img src="figures/2-CreateProject.png" alt="2-CreateProject" style="zoom:50%;" />

如上图所示，在IDE主页选择Create Project，选择[Lite]Empty Ability模板后，点击Next。

【也可以在IDE主页选择Open Project，然后打开示例程序litejs7（即Lite系统的JS应用API7）】

<img src="figures/3-CreateProject.png" alt="3-CreateProject" style="zoom:50%;" />

如上图所示，配置好项目信息，注意这里的API版本只能选择API7，而FA和JS也是不可修改的，配置好之后点击Finish即可自动创建工程。

打开工程代码  ...\socket_demo\entry\src\main\config.json, 将其中deviceType字段的值修改为smartVision：

<img src="figures/4-DeviceType.png" alt="4-DeviceType" style="zoom:80%;" />

然后根据需要修改其他的源代码和资源文件以适应项目需求。



在需要用到本文档介绍的socket接口时，需要将本文档同目录下的**@system.socket.d.ts**文件复制到第一张图中的OpenHarmony SDK安装路径下的如下目录中：D:\Program\Huawei\SDKohos\7\js\api\phone\，然后在使用socket接口的js文件中 **import socket from '@system.socket';** 然后使用即可。



## Socket接口列表

**@system.socket.d.ts**文件中声明了JS应用程序可以调用的接口和参数，具体功能则由OH系统的ace_lite组件通过调用标准库的socket接口来实现。



## Socket接口的使用

请参考示例程序 litejs7.rar 中的使用方法进行确认。



## 测试

在PC端使用TCP/UDP Socket调试工具软件（如 SocketTool.exe 等）创建TCP/UDP服务器，指定端口号；

在JS应用中修改服务器IP和端口号，重新编译出hap应用（签名或未签名），安装到开发板中，通过应用界面的按钮开始测试流程，通过终端工具查看日志确认Socket通信过程正确即可【测试用例的日志见 socket_test_log.txt】。

<img src="figures/5-JSAPP.jpg" alt="5-JSAPP" style="zoom: 33%;" />

## 注意事项

TCP/UDP client接收从服务器发送过来的消息时，目前在ace_lite组件中固定了最大为1024字节(包含结尾的'\\0'字符，因此可接收的最大消息size为1023 Bytes)，因此在JS应用程序中配置的 recvBufSize 取值范围为[1,1024]，如需增大这个Buffer Size，请联系我进行更改和重新输出。【下一步要改为由JS应用传下的参数来指定Buffer Size】

使用过程中发现问题，或者有新的功能需求，可在[这个仓库](https://gitee.com/hihope-loongarch/images)提交缺陷和需求。