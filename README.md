# README

本仓库存放润和软件（润开鸿）适配OpenHarmony（基于3.2Beta3版本）到龙芯方案SoC（loongarch64指令集架构）开发板上的可烧录镜像。

## 开发板说明

### dayu400

dayu400开发板（SoC：2K0500，板上丝印型号：DAYU400-MB_V0.1），是润和软件（润开鸿）基于广东龙芯火龙板（SoC：2K0500，板上丝印型号：LS_GD_1A500_MB_V1.0）进行开发的开发板，适配OpenHarmony小型系统。默认适配正点原子的ATK-4.3 RGBLCD显示屏（ALIENTEK  4.3' RGBLCD 480x800）。

```
CPU:   LA264
Speed: Cpu @ 600 MHz/ Mem @ 400 MHz/ Bus @ 100 MHz
Model: loongson-2k500
Board: LS2K500-DAYU400-MB
DRAM:  1 GiB
Core:  62 devices, 24 uclasses, devicetree: board
NAND:  using 4-bit/512 bytes BCH ECC
device found, Manufacturer ID: 0xec, Chip ID: 0xdc
Samsung NAND 512MiB 3,3V 8-bit
512 MiB, SLC, erase size: 128 KiB, page size: 2048, OOB size: 64
512 MiB
```

<img src="figures/dayu400-开发板.jpg" alt="dayu400-开发板" style="zoom: 60%;" />

### dayu401

dayu401开发板是广东龙芯迷你板（SoC：2K0500，板上丝印型号：LS2K0500_DEV_BOARD_V1.0），由润和软件（润开鸿）适配OpenHarmony小型系统。默认适配正点原子的ATK-4.3 RGBLCD显示屏（ALIENTEK  4.3' RGBLCD 480x800）。

```
CPU:   LA264
Speed: Cpu @ 600 MHz/ Mem @ 400 MHz/ Bus @ 100 MHz
Model: loongson-2k500
Board: LS2K500-MINI-DP
DRAM:  512 MiB
Core:  51 devices, 22 uclasses, devicetree: board
NAND:  using 4-bit/512 bytes BCH ECC
device found, Manufacturer ID: 0x2c, Chip ID: 0xda
Micron NAND 256MiB 3,3V 8-bit
256 MiB, SLC, erase size: 128 KiB, page size: 2048, OOB size: 64
256 MiB
```

<img src="figures/dayu401-开发板接口.jpg" alt="dayu401-开发板接口" style="zoom: 100%;" />

### dayu410

dayu410开发板是广东龙芯星云板（SoC：2K1000，板上丝印型号：JL_LSGD2K10-DP1_V1.0），由润和软件（润开鸿）适配OpenHarmony小型系统。默认适配正点原子的ATK-MD0700R显示屏（ALIENTEK  7' RGBLCD 1024x600）。

```
--[LS2K1000-JinLong-MB-V1.3]--
```

![dayu410-开发板接口](figures/dayu410-开发板接口.png)

## 镜像说明

### dayu400_3.2B3.tar.gz 

### dayu401_3.2B3.tar.gz 

> uImage：内核镜像
>
> rootfs-ubifs-ze.img：根文件系统

这两个压缩包分别是dayu400和dayu401开发板基于OpenHarmony 3.2Beta3版本的可烧录镜像，两块开发板的镜像可以互相交叉烧录，但由于两块开发板各自的触摸控制模块的RST和INT信号所连接GPIO引脚不同，需要烧录正确的镜像才有触摸功能。

### dayu410_3.2B3.tar.gz 

> uImage：内核镜像
>
> ramdisk.gz：ramdisk镜像
>
> rootfs.tar.gz：根文件系统

这个压缩包是dayu410开发板基于OpenHarmony 3.2Beta3版本的可烧录镜像，

## 烧录dayu400和dayu401的镜像

### USB方式烧录内核与rootfs

准备一个fat32格式的U盘，在根目录下创建 “update”目录，将压缩包内的两个可烧录镜像文件复制到U盘的“update”目录内备用。

在开发板上插入上述U盘，上电到uboot倒计时处，按m键，进入一级菜单：

```
  *** U-Boot Boot Menu ***

     [1] System boot select
     [2] Update kernel
     [3] Update rootfs
     [4] Update u-boot
     [5] Update ALL
     [6] System install or recover
     [7] U-Boot console

  Press UP/DOWN to move, ENTER to select, ESC/CTRL+C to quit
```

上下键选择"[2] Update kernel"，回车确认，进入二级菜单：

```
  *** U-Boot Boot Menu ***

     [1] Update kernel (uImage) to nand flash (by usb)
     [2] Update kernel (uImage) to nand flash (by tftp)
     [3] Return

  Press UP/DOWN to move, ENTER to select
```

上下键选择"[1] Update kernel (uImage) to nand flash (by usb)"，回车确认，即可烧录内核镜像；

烧录内核镜像完成后，会有如下提示：

```
######################################################
### update target: kernel
### update way   : usb
### update result: success
######################################################

=>
```

此时输入“bootmenu”，回车确认，可以回到一级菜单，上下键选择"[3] Update rootfs"，回车确认，进入二级菜单：

```
  *** U-Boot Boot Menu ***

     [1] Update rootfs (rootfs-ubifs-ze.img) to nand flash (by usb)
     [2] Update rootfs (rootfs-ubifs-ze.img) to nand flash (by tftp)
     [3] Return

  Press UP/DOWN to move, ENTER to select
```

上下键选择"[1] Update kernel (rootfs-ubifs-ze.img) to nand flash (by usb)"，回车确认，即可烧录根文件系统镜像。

完成烧录后，重启系统即可。

### tftp方式烧录内核与rootfs

先在windows或linux下搭建tftp服务器，将压缩包内的两个可烧录镜像文件复制到tftp共享目录下，开发板配置好tftp服务器地址和开发板的IP地址等必要信息。

在开发板上电到uboot倒计时处，按m键，进入一级菜单，分别选择烧录这两个镜像，类似USB烧录方式，只需选择通过tftp烧录这两个镜像即可。




## 烧录dayu410的镜像

### USB方式烧录内核与rootfs

准备一个fat32格式的U盘，在根目录下创建 “install”目录，将压缩包内的3个可烧录镜像文件复制到U盘的“install”目录内备用。

在开发板上插入上述U盘，上电到uboot倒计时处，按m键，进入一级菜单：

```
  *** U-Boot Boot Menu ***

     [1] System boot select
     [2] Update kernel
     [3] Update rootfs
     [4] Update u-boot
     [5] Update ALL
     [6] System install or recover
     [7] U-Boot console

  Press UP/DOWN to move, ENTER to select, ESC/CTRL+C to quit
```

上下键选择"[6] System install or recover"，回车确认，显示如下的二级菜单：

```
  *** U-Boot Boot Menu ***

     [1] System install to sata disk (by usb)
     [2] System install to sata disk (by usb iso)
     [3] System install to sata disk (by tftp)
     [4] System recover from sata disk
     [5] Return

  Press UP/DOWN to move, ENTER to select
```

上下键选择"[1] System install to sata disk (by usb)"，回车确认后，系统自动重启，并进入系统升级模式(会同时升级内核和rootfs)，串口打印如下的日志：

```
..............
[    5.977932] This architecture does not have kernel memory protection.
[    5.984475] Run /linuxrc as init process
Starting syslogd: OK
Starting klogd: OK
Running sysctl: OK
Starting telnetd: OK
Processing /etc/profile...
we will run three script symbol three stage
some stage will waste a few minutes. don't shutdowm until finish!
Install System...

*************************************************************************
*********************************STARG 1*********************************
************this stage for format SSD and copy file to backup************
*************************************************************************

[   10.218630] FAT-fs (sdb1): Volume was not properly unmounted. Some data may be corrupt. Please run fsck.
-------------> stage1 mount /dev/sdb1 success <-------------
-------------> stage1 check_file_for_safe <-------------

-------------> stage1.1 fdisk disk <-------------

would split sda to 2 partition! /dev/sda1 and /dev/sda3
[   13.260970]  sda: sda1 sda3

Disk /dev/sda: 30 GB, 32017047552 bytes, 62533296 sectors
/dev/sda1    261,22,17   1023,254,63    4194367   62533295   58338929 27.8G 83 Linux
/dev/sda3    0,1,1       261,22,16           63    4194366    4194304 2048M 82 Linux swap

-------------> stage1.2 format / partition --- start <-------------
mke2fs 1.45.6 (20-Mar-2020)
-------------> stage1.2 format / partition --- success <-------------
-------------> stage1.2 format swap partition --- start <-------------
-------------> stage1.2 format swap partition --- success <-------------

-------------> stage1.3: copy file to SSD <-------------
[   21.378981] EXT4-fs (sda1): mounted filesystem with ordered data mode. Opts: (null)
      -----> copy uImage
'/mnt/usb0/install/uImage' -> '/mnt/usb1/uImage'
      -----> copy rootfs.tar.gz (wait a few minutes)
rootfs.tar.gz
         32,768   0%    0.00kB/s    0:00:00  [   23.288659] random: crng init done
     34,673,912 100%    4.75MB/s    0:00:06 (xfr#1, to-chk=0/1)
-------------> stage1 end <-------------

*************************************************************************
*********************************STARG 2*********************************
*******************uzip system and setup it with fstab*******************
*************************************************************************

-------------> stage2.1 unzip system <-------------
[   32.594528] EXT4-fs (sda1): mounted filesystem with ordered data mode. Opts: (null)
can observe process in screen

-------------> stage2.2 copy uImage to /boot <-------------
[   53.403508] EXT4-fs (sda1): mounted filesystem with ordered data mode. Opts: (null)

-------------> stage2.3 copy fstab to /etc/fstab <-------------
[   53.500146] EXT4-fs (sda1): mounted filesystem with ordered data mode. Opts: (null)

-------------> stage2 end <-------------

*************************************************************************
*********************************STARG 3*********************************
*******************this stage for check log and reboot*******************
*************************************************************************

-------------> stage3 install system finish! ple[   53.917811] sd 0:0:0:0: [sda] Synchronizing SCSI cache
ase reboot or shutdowm! <-------------
[   53.927748] reboot: Restarting system
```

升级完成会自动重启，进入OpenHarmony系统桌面。

### tftp方式烧录内核与rootfs

先在windows或linux下搭建tftp服务器，将压缩包内的3个可烧录镜像文件复制到tftp共享目录下，开发板配置好tftp服务器地址和开发板的IP地址等必要信息。

在开发板上电到uboot倒计时处，按m键，进入一级菜单：

```
  *** U-Boot Boot Menu ***

     [1] System boot select
     [2] Update kernel
     [3] Update rootfs
     [4] Update u-boot
     [5] Update ALL
     [6] System install or recover
     [7] U-Boot console

  Press UP/DOWN to move, ENTER to select, ESC/CTRL+C to quit
```

上下键选择"[6] System install or recover"，回车确认，显示如下的二级菜单：

```
  *** U-Boot Boot Menu ***

     [1] System install to sata disk (by usb)
     [2] System install to sata disk (by usb iso)
     [3] System install to sata disk (by tftp)
     [4] System recover from sata disk
     [5] Return

  Press UP/DOWN to move, ENTER to select
```

上下键选择"[3] System install to sata disk (by tftp)"，回车确认后，系统自动重启，并进入系统升级模式(会同时升级内核和rootfs)，通过tftp方式升级系统相较于USB方式会慢很多。